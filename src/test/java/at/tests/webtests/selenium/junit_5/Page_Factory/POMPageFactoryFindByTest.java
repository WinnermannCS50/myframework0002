package at.tests.webtests.selenium.junit_5.Page_Factory;

import at.web.selenium.junit_5.Page_Factory.BrowserFactory;
import at.web.selenium.junit_5.Page_Factory.POMPageFactoryFindBy;
import io.qameta.allure.*;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class POMPageFactoryFindByTest {
    private static WebDriver driver;
    //WebDriver driver;

    @BeforeAll
    public static void beforeAll(){
        driver = BrowserFactory.StartBrowser("chrome", "http://www.edureka.co/");

    }

    @Step("Cценарий: Compare Titles")
    @Order(1)
    @Test
    @DisplayName("Cценарий: Compare Titles")
    public void checkTitleEdureka() throws InterruptedException {
        //Step1
        /**
         * В данном случае чтобы Войти нужно
         * перед URL подставить логин и пароль
         * admin:admin@
         * полная версия "http://admin:admin@the-internet.herokuapp.com/basic_auth"
         */
        //driver = BrowserFactory.StartBrowser("chrome", "http://www.edureka.co/");
        POMPageFactoryFindBy checkTitleEdureka = PageFactory.initElements(driver, POMPageFactoryFindBy.class);
        checkTitleEdureka.checkTitleEdureka();
    }

    @Order(2)
    @Test
    @DisplayName("Cценарий: Попытка Авторизации")
    public void login() throws InterruptedException {
        //Step2
        //This will launch browser with specific URL
        //Запускает нужный браузер из класса BrowserFactory с нужным URL
        //driver = BrowserFactory.StartBrowser("chrome", "http://the-internet.herokuapp.com");
        //Created PageObject using PageFactory
        //Создана страница POMPageFactoryFindBy в виде объекта checkBox
        POMPageFactoryFindBy login = PageFactory.initElements(driver, POMPageFactoryFindBy.class);
        //Call the method login()
        //Вызывает метод login()
        login.login();

    }

    @Order(3)
    @Test
    @DisplayName("Тест3")
    public void test3(){
        //Step3
        /**
         * В данном случае чтобы Войти нужно
         * перед URL подставить логин и пароль
         * admin:admin@
         * полная версия "http://admin:admin@the-internet.herokuapp.com/digest_auth"
         */
        //driver = BrowserFactory.StartBrowser("chrome", "http://admin:admin@the-internet.herokuapp.com/digest_auth");
        POMPageFactoryFindBy test3 = PageFactory.initElements(driver, POMPageFactoryFindBy.class);
        test3.test3();
    }

    @Order(4)
    @Test
    @DisplayName("Тест4")
    public void test4(){
        //Step4
        POMPageFactoryFindBy test4 = PageFactory.initElements(driver, POMPageFactoryFindBy.class);
        test4.test4();

    }

    @Order(5)
    @Test
    @DisplayName("Тест5")
    public void test5(){
        //Step5
        //driver = BrowserFactory.StartBrowser("chrome", "http://the-internet.herokuapp.com/drag_and_drop");
        POMPageFactoryFindBy test5 = PageFactory.initElements(driver, POMPageFactoryFindBy.class);
        test5.test5();

    }

    @Order(6)
    @Disabled("Этот тест будет исключен из прогона")
    @Test
    @DisplayName("Тест6")
    public void test6(){
        //Step6
        POMPageFactoryFindBy test6 = PageFactory.initElements(driver, POMPageFactoryFindBy.class);
        test6.test6();

    }

    @Order(7)
    @Test
    @DisplayName("Тест7")
    public void test7(){
        //Step7
        POMPageFactoryFindBy test7 = PageFactory.initElements(driver, POMPageFactoryFindBy.class);
        test7.test7();

    }

    @AfterAll
    public static void afterAll(){
        POMPageFactoryFindBy afterAll = PageFactory.initElements(driver, POMPageFactoryFindBy.class);
        afterAll.afterAll();

    }
}
