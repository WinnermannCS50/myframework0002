package at.tests.webtests.selenide.junit_5;

import at.web.selenide.junit_5.TheInternetHerokuAppUICheckBox;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TheInternetHerokuAppUITest {

    @BeforeAll
    public static void startBrowser(){
        System.out.println("startBrowser");
        TheInternetHerokuAppUICheckBox.startBrowser();
    }

    @Order(1)
    @Test
    @DisplayName("Cценарий: Проверка чек-бокса")
    public void checkBox() {
        TheInternetHerokuAppUICheckBox.checkBox();
    }

    @Order(2)
    @Test
    @DisplayName("Тест2")
    public void test2(){
        TheInternetHerokuAppUICheckBox.test2();

    }

    @AfterAll
    public static void closeBrowser(){
        TheInternetHerokuAppUICheckBox.closeBrowser();
        System.out.println("closeBrowser");

    }
}
