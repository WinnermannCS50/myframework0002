package at.api.JacksonJsonPOJO.jsonparsing.pojo;

//Это POJO
public class SimpleTestCaseJsonPOJO {

    //Атрибут title
    private String title;

    //Геттер
    public String getTitle() {
        return title;
    }

    //Сеттер
    public void setTitle(String title) {
        this.title = title;
    }
}
