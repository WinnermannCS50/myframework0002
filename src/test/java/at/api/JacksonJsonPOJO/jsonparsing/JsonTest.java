package at.api.JacksonJsonPOJO.jsonparsing;

import at.api.JacksonJsonPOJO.jsonparsing.pojo.SimpleTestCaseJsonPOJO;
import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Этот класс парсит Объект simpleTestCaseJsonSource
 * Используется JUnit5 и Jackson Json
 */

class JsonTest {
    //Объект simpleTestCaseJsonSource
    private String simpleTestCaseJsonSource = "{\"title\" : \"Coder From Scratch\"}";

    @Test
    void parse() throws IOException {
        //парсим Объект simpleTestCaseJsonSource
        JsonNode node = Json.parse(simpleTestCaseJsonSource);
        //Вывод в консоль node - результата распарсивания Объекта simpleTestCaseJsonSource
        System.out.println(node.get("title").asText());

        assertEquals("Coder From Scratch",node.get("title").asText());
    }

    //Тест для POJO
    @Test
    void fromJson() throws IOException {
        //парсим Объект simpleTestCaseJsonSource
        JsonNode node = Json.parse(simpleTestCaseJsonSource);
        //Создает объект pojo с атрибутом title
        SimpleTestCaseJsonPOJO pojo = Json.fromJson(node, SimpleTestCaseJsonPOJO.class);
        //Вывод в консоль pojo.getTitle()
        System.out.println("POJO Title : " + pojo.getTitle());

        assertEquals("Coder From Scratch",pojo.getTitle());

    }
}