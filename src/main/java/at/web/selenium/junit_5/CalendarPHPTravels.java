package at.web.selenium.junit_5;

import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalendarPHPTravels {
    public static String baseUrl = "https://phptravels.net/";
    static String driverPath = "src/test/resources/chromedriver.exe";
    public static WebDriver driver;


    @BeforeAll
    public static void openBrowser(){
        System.setProperty("webdriver.chrome.driver", driverPath);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(baseUrl);
    }

    @Order(1)
    //@Disabled("Этот тест будет исключен из прогона1")
    @Test
    @DisplayName("Тест1")
    public void test1() throws InterruptedException {
        String month = "March, 2021";
        String year = "2021";
        String day = "20";

        //нажать на поле "hotels"
        driver.findElement(By.xpath("//a[@data-name='hotels']")).click();
        Thread.sleep(2000);
        //нажать на поле "checkin"
        driver.findElement(By.id("checkin")).click();
        Thread.sleep(2000);

        while (true){
            //получить текст с плашки "March, 2021"
            String text = driver.findElement(By.xpath("//div[@class ='datepicker--nav-title' and text()='March, ']/i[ text()='2021']")).getText();
            System.out.println(text);
            if (text.equals(year)){
                break;

            }
            else {
                //нажать на стрелочку вправо "next month"
                driver.findElement(By.xpath("//div[@class='datepicker--nav-action'][@data-action='next']")).click();
                Thread.sleep(2000);

            }
        }

        //нажать на дату
        driver.findElement(By.xpath("//*[@id=\"datepickers-container\"]/div[1]/div/div/div[2]/div[21]")).click();
        Thread.sleep(8000);

    }

    @Order(2)
    @Disabled("Этот тест будет исключен из прогона2")
    @Test
    @DisplayName("Тест2")
    public void test2(){
        String[] massive = new String[2];
        massive[0] = "March, ";
        massive[1] = "2021";
        System.out.println(massive[0]);
        System.out.println(massive[1]);

    }

    @Order(3)
    @Disabled("Этот тест будет исключен из прогона3")
    @Test
    @DisplayName("Тест3")
    public void test3() throws InterruptedException {
        String month = "March, 2021";
        String year = "2021";
        String day = "20";

        //нажать на поле "hotels"
        driver.findElement(By.xpath("//a[@data-name='hotels']")).click();
        Thread.sleep(2000);
        //нажать на поле "checkin"
        driver.findElement(By.id("checkin")).click();
        Thread.sleep(2000);

        //получить текст с плашки "March, 2021"
        String text = driver.findElement(By.xpath("//div[@class ='datepicker--nav-title' and text()='March, ']/i[ text()='2021']")).getText();
        System.out.println(text);

        //получить текст с плашки "March, 2021"
        String text2 = driver.findElement(By.xpath("//*[@id=\"datepickers-container\"]/div[1]/nav/div[2]")).getText();
        System.out.println(text2);

    }

    @Order(4)
    @Disabled("Этот тест будет исключен из прогона4")
    @Test
    @DisplayName("Тест4")
    public void test4(){
        String[] month = new String[2];
        month[0] = "March,";
        month[1] = "2021";

        //String month = "March,";
        //String year = "2021";
        //String day = "20";

        HashMap map = new HashMap();
        map.put("month", "March,");
        map.put("month", "2021");
        //map.put("year", "2021");

        System.out.println(map.toString());

    }

    @AfterAll
    public static void closeBrowser(){
        driver.close();
    }

}
