package at.web.selenium.junit_5.Page_Factory;

import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class POMPageFactoryFindBy {

    WebDriver driver;

    //ldriver - local driver

    public POMPageFactoryFindBy(WebDriver ldriver) {
        this.driver = ldriver;
    }

    @FindBy(how = How.LINK_TEXT,using = "Log In")
    WebElement Login;

    @FindBy(how = How.ID,using = "si_popup_email")
    WebElement userName;

    @FindBy(how = How.ID,using = "si_popup_passwd")
    @CacheLookup //Кэширует объект, что бы повторно не искать на странице
    WebElement password;

    @FindBy(how = How.XPATH,using = "//button[@class='clik_btn_log btn-block']")
    WebElement Next;

    @FindBy(how = How.ID,using = "passwdErrorr")
    WebElement errorMessage;



    public void checkTitleEdureka(){

        System.out.println("Первый тест JUNIT5");
        System.out.println("checkTitleEdureka");
        String expectedTitle = "Instructor-Led Online Training with 24X7 Lifetime Support | Edureka";
        String actualTitle = driver.getTitle();
        System.out.println(actualTitle);
        assertEquals(actualTitle, expectedTitle);

    }

    //@Step("Cценарий: Попытка Авторизации")
    public void login() throws InterruptedException {

        System.out.println("test2");
        //нажать на кнопку "Log In"
        Login.click();
        Thread.sleep(4000);
        //ввести валидное userName
        userName.sendKeys("omkar.hiremath@edureka.co");
        Thread.sleep(4000);
        //ввести невалидный password
        password.sendKeys("12345678");
        Thread.sleep(6000);
        //нажать кнопку Next
        Next.click();
        Thread.sleep(10000);
        System.out.println(errorMessage.getText());
        String errorMessageExpected = "Sign In Failed. Invalid login credentials.";
        //убедиться, что отобразилось сообщение об ошибке с ожидаемым текстом
        assertEquals(errorMessage.getText(), errorMessageExpected);
    }

    //@Step("Тест3")
    public void test3(){

        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test3");
    }

    //@Step("Тест4")
    public void test4(){
        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test4");
    }

    //@Step("Тест5")
    public void test5() {

        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test5");
    }

    //@Step("Тест6")
    public void test6(){

        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test6");

    }

    //@Step("Тест7")
    public void test7() {

        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test7");
    }

    public void afterAll(){
        System.out.println("afterAll");
        driver.close();
    }

}
