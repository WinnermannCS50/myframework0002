package at.web.selenide.junit_5;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TheInternetHerokuAppUICheckBox {
    public static void startBrowser(){
        //Открыть браузер
        System.setProperty("selenide.browser", "chrome");
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.timeout = 6000;
        open("http://the-internet.herokuapp.com");

    }

    public static void checkBox(){

        //Убедиться что на странице есть слова "Welcome to the-internet"
        element(By.cssSelector("#content h1")).shouldHave(text("Welcome to the-internet"));
        //Убедиться что ссылка содержит слова "Checkboxes" и перейти по ссылке
        element(By.cssSelector("#content li:nth-child(6) a")).shouldHave(text("Checkboxes")).click();

        //Проверяет что Чек-бокс не выбран
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)")).shouldNotBe(selected);
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)")).shouldNotBe(checked);
        //Проставляет Чек-бокс
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)")).setSelected(true);
        //Проверяет что Чек-бокс выбран
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)")).shouldBe(selected);
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(1)")).shouldBe(checked);

        //Проверяет что Чек-бокс выбран
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)")).shouldBe(selected);
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)")).shouldBe(checked);
        //Снимает Чек-бокс
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)")).setSelected(false);
        //Проверяет что Чек-бокс не выбран
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)")).shouldNotBe(selected);
        $(By.cssSelector("#checkboxes input[type=checkbox]:nth-child(3)")).shouldNotBe(checked);
    }

    public static void test2(){
        int expected = 1;
        int actual = 1;
        assertEquals(expected, actual);
        System.out.println("test2");
    }

    public static void closeBrowser(){
        Selenide.clearBrowserCookies();
        Selenide.closeWindow();
    }
}
