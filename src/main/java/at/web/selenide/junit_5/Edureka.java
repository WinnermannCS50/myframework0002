package at.web.selenide.junit_5;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import static com.codeborne.selenide.Selenide.open;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Edureka {
    public static void startBrowser(){
        //Открыть браузер
        System.setProperty("selenide.browser", "chrome");
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;
        Configuration.timeout = 100000;
        open("http://www.edureka.co/");

    }

    public static void checkTitleEdureka(){
        System.out.println("Первый тест JUNIT5");
        System.out.println("checkTitleEdureka");
        String expectedTitle = "Instructor-Led Online Training with 24X7 Lifetime Support | Edureka";
        String actualTitle = Selenide.title();
        System.out.println(actualTitle);
        assertEquals(actualTitle, expectedTitle);
    }

    public static void closeBrowser(){
        Selenide.clearBrowserCookies();
        Selenide.closeWindow();
    }
}
