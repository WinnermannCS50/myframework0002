package at.api;

import at.common.Config.CommonSpec;
import io.restassured.response.Response;
import org.junit.jupiter.api.*;

import java.io.File;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class HerokuAppAPITest {
    private static final String HTTP_THE_INTERNET_HEROKUAPP_COM_UPLOAD = "http://the-internet.herokuapp.com/upload";

    @Order(1)
    @Test
    @DisplayName("Cценарий: Загрузить на сервер uploadFile.txt")
    public void uploadFile(){
        System.out.println("uploadTerminalDictionary");

        File file = new File("upload/uploadFile.txt");

        Response response;
        response = given().
                contentType("multipart/form-data").
                with().
                multiPart("file", file, "multipart/form-data").
                when().
                post(HTTP_THE_INTERNET_HEROKUAPP_COM_UPLOAD);

        response.then().
                statusCode(200).
                //body(containsString("{\"message\":\"Справочник терминалов загружен\",\"msg\":\"Справочник терминалов загружен\",\"success\":true}")).
                log().all();

    }
}
