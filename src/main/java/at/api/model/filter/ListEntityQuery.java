package at.api.model.filter;

/**
 * Представление запроса на получение данных
 */
public class ListEntityQuery {
    private Integer page;
    private Integer limit;
    //private String sort;
    private String reportColumnDesc;
    private FieldFilter[] filter;
    private String reportFormat;

    /**
     * Создать запрос по умолчанию
     *
     * @param fieldFilter фильтры для полей.
     * @return запрос на получение данных
     */

    public static ListEntityQuery defaultWith(FieldFilter... fieldFilter){
        return new ListEntityQuery()
                .setPage(1)
                .setLimit(25)
                .setFilter(fieldFilter);
    }

    public Integer getPage() {
        return page;
    }

    public ListEntityQuery setPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getLimit() {
        return limit;
    }

    public ListEntityQuery setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public String getReportColumnDesc() {
        return reportColumnDesc;
    }

    public ListEntityQuery setReportColumnDesc(String reportColumnDesc) {
        this.reportColumnDesc = reportColumnDesc;
        return this;
    }

    public FieldFilter[] getFilter() {
        return filter;
    }

    public ListEntityQuery setFilter(FieldFilter[] filter) {
        this.filter = filter;
        return this;
    }

    public String getReportFormat() {
        return reportFormat;
    }

    public ListEntityQuery setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
        return this;
    }
}
