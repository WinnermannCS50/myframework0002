package at.api.model.result;

import java.util.List;

public class ListResultData {
    private List<T> results;
    private int total;
    private String msg;
    private String message;
    private String stackTrace;
    private boolean success;

    public List<T> getResults() {
        return results;
    }

    public ListResultData setResults(List<T> results) {
        this.results = results;
        return this;
    }

    public int getTotal() {
        return total;
    }

    public ListResultData setTotal(int total) {
        this.total = total;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public ListResultData setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ListResultData setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public ListResultData setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public ListResultData setSuccess(boolean success) {
        this.success = success;
        return this;
    }
}
