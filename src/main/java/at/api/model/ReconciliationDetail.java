package at.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class ReconciliationDetail {
    private String parentObject_reconciliationResult;
    private String deviceId;
    private String terminalType;
    private String parentObject_subdivision_bisCode;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime parentObject_createDate;

    private String parentObject_subdivision_bik2018;
    private Integer currencyCode;
    private String operationType;
    private Integer mkCredit;
    private String parentObject_subdivision_branchCode;
    private String parentObject_subdivision_bik;
    private String parentObject_reconciliationProgress;
    private Integer id;
    private Integer mkDebit;
    private Integer icCredit;
    private Integer icDebit;
    private Integer sdcCredit;
    private Integer sdcDebit;
    private String parentObject_subdivision_shortName;
    private String bisquiteCredit;
    private String bisquiteDebit;
    private String reconciliationResult;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime parentObject_reconciliationDate;

    private Integer parentObject_subdivision_limitForNotify;
    private String comment;
    private Integer parentObject_id;
    private String parentObject_subdivision_fullName;
    private String parentObject_userLogin;
    private String reportFormat;
    private String filter;
    private String sort;

    public String getParentObject_reconciliationResult() {
        return parentObject_reconciliationResult;
    }

    public ReconciliationDetail setParentObject_reconciliationResult(String parentObject_reconciliationResult) {
        this.parentObject_reconciliationResult = parentObject_reconciliationResult;
        return this;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public ReconciliationDetail setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public ReconciliationDetail setTerminalType(String terminalType) {
        this.terminalType = terminalType;
        return this;
    }

    public String getParentObject_subdivision_bisCode() {
        return parentObject_subdivision_bisCode;
    }

    public ReconciliationDetail setParentObject_subdivision_bisCode(String parentObject_subdivision_bisCode) {
        this.parentObject_subdivision_bisCode = parentObject_subdivision_bisCode;
        return this;
    }

    public LocalDateTime getParentObject_createDate() {
        return parentObject_createDate;
    }

    public ReconciliationDetail setParentObject_createDate(LocalDateTime parentObject_createDate) {
        this.parentObject_createDate = parentObject_createDate;
        return this;
    }

    public String getParentObject_subdivision_bik2018() {
        return parentObject_subdivision_bik2018;
    }

    public ReconciliationDetail setParentObject_subdivision_bik2018(String parentObject_subdivision_bik2018) {
        this.parentObject_subdivision_bik2018 = parentObject_subdivision_bik2018;
        return this;
    }

    public Integer getCurrencyCode() {
        return currencyCode;
    }

    public ReconciliationDetail setCurrencyCode(Integer currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public String getOperationType() {
        return operationType;
    }

    public ReconciliationDetail setOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    public Integer getMkCredit() {
        return mkCredit;
    }

    public ReconciliationDetail setMkCredit(Integer mkCredit) {
        this.mkCredit = mkCredit;
        return this;
    }

    public String getParentObject_subdivision_branchCode() {
        return parentObject_subdivision_branchCode;
    }

    public ReconciliationDetail setParentObject_subdivision_branchCode(String parentObject_subdivision_branchCode) {
        this.parentObject_subdivision_branchCode = parentObject_subdivision_branchCode;
        return this;
    }

    public String getParentObject_subdivision_bik() {
        return parentObject_subdivision_bik;
    }

    public ReconciliationDetail setParentObject_subdivision_bik(String parentObject_subdivision_bik) {
        this.parentObject_subdivision_bik = parentObject_subdivision_bik;
        return this;
    }

    public String getParentObject_reconciliationProgress() {
        return parentObject_reconciliationProgress;
    }

    public ReconciliationDetail setParentObject_reconciliationProgress(String parentObject_reconciliationProgress) {
        this.parentObject_reconciliationProgress = parentObject_reconciliationProgress;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public ReconciliationDetail setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getMkDebit() {
        return mkDebit;
    }

    public ReconciliationDetail setMkDebit(Integer mkDebit) {
        this.mkDebit = mkDebit;
        return this;
    }

    public Integer getIcCredit() {
        return icCredit;
    }

    public ReconciliationDetail setIcCredit(Integer icCredit) {
        this.icCredit = icCredit;
        return this;
    }

    public Integer getIcDebit() {
        return icDebit;
    }

    public ReconciliationDetail setIcDebit(Integer icDebit) {
        this.icDebit = icDebit;
        return this;
    }

    public Integer getSdcCredit() {
        return sdcCredit;
    }

    public ReconciliationDetail setSdcCredit(Integer sdcCredit) {
        this.sdcCredit = sdcCredit;
        return this;
    }

    public Integer getSdcDebit() {
        return sdcDebit;
    }

    public ReconciliationDetail setSdcDebit(Integer sdcDebit) {
        this.sdcDebit = sdcDebit;
        return this;
    }

    public String getParentObject_subdivision_shortName() {
        return parentObject_subdivision_shortName;
    }

    public ReconciliationDetail setParentObject_subdivision_shortName(String parentObject_subdivision_shortName) {
        this.parentObject_subdivision_shortName = parentObject_subdivision_shortName;
        return this;
    }

    public String getBisquiteCredit() {
        return bisquiteCredit;
    }

    public ReconciliationDetail setBisquiteCredit(String bisquiteCredit) {
        this.bisquiteCredit = bisquiteCredit;
        return this;
    }

    public String getBisquiteDebit() {
        return bisquiteDebit;
    }

    public ReconciliationDetail setBisquiteDebit(String bisquiteDebit) {
        this.bisquiteDebit = bisquiteDebit;
        return this;
    }

    public String getReconciliationResult() {
        return reconciliationResult;
    }

    public ReconciliationDetail setReconciliationResult(String reconciliationResult) {
        this.reconciliationResult = reconciliationResult;
        return this;
    }

    public LocalDateTime getParentObject_reconciliationDate() {
        return parentObject_reconciliationDate;
    }

    public ReconciliationDetail setParentObject_reconciliationDate(LocalDateTime parentObject_reconciliationDate) {
        this.parentObject_reconciliationDate = parentObject_reconciliationDate;
        return this;
    }

    public Integer getParentObject_subdivision_limitForNotify() {
        return parentObject_subdivision_limitForNotify;
    }

    public ReconciliationDetail setParentObject_subdivision_limitForNotify(Integer parentObject_subdivision_limitForNotify) {
        this.parentObject_subdivision_limitForNotify = parentObject_subdivision_limitForNotify;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public ReconciliationDetail setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public Integer getParentObject_id() {
        return parentObject_id;
    }

    public ReconciliationDetail setParentObject_id(Integer parentObject_id) {
        this.parentObject_id = parentObject_id;
        return this;
    }

    public String getParentObject_subdivision_fullName() {
        return parentObject_subdivision_fullName;
    }

    public ReconciliationDetail setParentObject_subdivision_fullName(String parentObject_subdivision_fullName) {
        this.parentObject_subdivision_fullName = parentObject_subdivision_fullName;
        return this;
    }

    public String getParentObject_userLogin() {
        return parentObject_userLogin;
    }

    public ReconciliationDetail setParentObject_userLogin(String parentObject_userLogin) {
        this.parentObject_userLogin = parentObject_userLogin;
        return this;
    }

    public String getReportFormat() {
        return reportFormat;
    }

    public ReconciliationDetail setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
        return this;
    }

    public String getFilter() {
        return filter;
    }

    public ReconciliationDetail setFilter(String filter) {
        this.filter = filter;
        return this;
    }

    public String getSort() {
        return sort;
    }

    public ReconciliationDetail setSort(String sort) {
        this.sort = sort;
        return this;
    }
}
