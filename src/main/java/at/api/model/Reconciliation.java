package at.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class Reconciliation {
    private String reconciliationResult;
    private String subdivision_shortName;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime reconciliationDate;

    private String userLogin;
    private String subdivision_reconciliationProgress;
    private String subdivision_fullName;
    private Integer subdivision_limitForNotify;
    private String subdivision_bik2018;
    private String subdivision_bisCode;
    private Integer id;
    private String Subdivision_bik;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime createDate;
    private String subdivision_branchCode;
    private String reportFormat;
    private String filter;
    private String sort;

    public String getReconciliationResult() {
        return reconciliationResult;
    }

    public Reconciliation setReconciliationResult(String reconciliationResult) {
        this.reconciliationResult = reconciliationResult;
        return this;
    }

    public String getSubdivision_shortName() {
        return subdivision_shortName;
    }

    public Reconciliation setSubdivision_shortName(String subdivision_shortName) {
        this.subdivision_shortName = subdivision_shortName;
        return this;
    }

    public LocalDateTime getReconciliationDate() {
        return reconciliationDate;
    }

    public Reconciliation setReconciliationDate(LocalDateTime reconciliationDate) {
        this.reconciliationDate = reconciliationDate;
        return this;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public Reconciliation setUserLogin(String userLogin) {
        this.userLogin = userLogin;
        return this;
    }

    public String getSubdivision_reconciliationProgress() {
        return subdivision_reconciliationProgress;
    }

    public Reconciliation setSubdivision_reconciliationProgress(String subdivision_reconciliationProgress) {
        this.subdivision_reconciliationProgress = subdivision_reconciliationProgress;
        return this;
    }

    public String getSubdivision_fullName() {
        return subdivision_fullName;
    }

    public Reconciliation setSubdivision_fullName(String subdivision_fullName) {
        this.subdivision_fullName = subdivision_fullName;
        return this;
    }

    public Integer getSubdivision_limitForNotify() {
        return subdivision_limitForNotify;
    }

    public Reconciliation setSubdivision_limitForNotify(Integer subdivision_limitForNotify) {
        this.subdivision_limitForNotify = subdivision_limitForNotify;
        return this;
    }

    public String getSubdivision_bik2018() {
        return subdivision_bik2018;
    }

    public Reconciliation setSubdivision_bik2018(String subdivision_bik2018) {
        this.subdivision_bik2018 = subdivision_bik2018;
        return this;
    }

    public String getSubdivision_bisCode() {
        return subdivision_bisCode;
    }

    public Reconciliation setSubdivision_bisCode(String subdivision_bisCode) {
        this.subdivision_bisCode = subdivision_bisCode;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public Reconciliation setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getGetSubdivision_bik() {
        return Subdivision_bik;
    }

    public Reconciliation setGetSubdivision_bik(String getSubdivision_bik) {
        this.Subdivision_bik = getSubdivision_bik;
        return this;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public Reconciliation setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public String getSubdivision_branchCode() {
        return subdivision_branchCode;
    }

    public Reconciliation setSubdivision_branchCode(String subdivision_branchCode) {
        this.subdivision_branchCode = subdivision_branchCode;
        return this;
    }

    public String getReportFormat() {
        return reportFormat;
    }

    public Reconciliation setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
        return this;
    }

    public String getFilter() {
        return filter;
    }

    public Reconciliation setFilter(String filter) {
        this.filter = filter;
        return this;
    }

    public String getSort() {
        return sort;
    }

    public Reconciliation setSort(String sort) {
        this.sort = sort;
        return this;
    }
}
