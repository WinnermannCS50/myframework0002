package at.api;

import at.api.model.Reconciliation;
import at.api.model.ReconciliationDetail;
import at.api.model.filter.ListEntityQuery;
import at.api.model.result.ListResultData;
import at.common.Config.CommonSpec;
import com.fasterxml.jackson.core.type.TypeReference;
import io.qameta.allure.Step;
import io.restassured.common.mapper.TypeRef;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;

import java.util.List;

import static io.restassured.RestAssured.given;

public class SDCSteps10554TC3 {
    private static final String SDC_TERMINAL_ATM_RECONCILIATION_LIST_JSON = "/sdc_terminal/atm/reconciliation/list.json";
    private static final String SDC_TERMINAL_ATM_RECONCILIATION_REPORT_DO = "/sdc_terminal/atm/reconciliation/report.do";
    private static final String SDC_TERMINAL_ATM_RECONCILIATION_DETAIL_UPDATE_DO = "/sdc_terminal/atm/reconciliation_detail/update.do";
    private static final String SDC_TERMINAL_ATM_RECONCILIATION_DETAIL_LIST_JSON = "/sdc_terminal/atm/reconciliation_detail/list.json";

    @Step("Сверка оборотов: делаем запрос с 01.10.2010 по 07.10.2020 'Головной офис'")
    public static void reconciliation(ListEntityQuery queryFilter){
        System.out.println("reconciliation");
        ListResultData<Reconciliation> result = CommonSpec.get("sdc_terminal")
                .queryParam("filter", (Object) queryFilter.getFilter())
                .queryParam("page", queryFilter.getPage())
                .queryParam("limit", queryFilter.getLimit())
                .when()
                .get(SDC_TERMINAL_ATM_RECONCILIATION_LIST_JSON)
                .then().statusCode(Matchers.anyOf(Matchers.equalTo(200), Matchers.equalTo(201)))
                .extract().as(new TypeRef<ListResultData<Reconciliation>>() {
                });
        Assertions.assertTrue(result.isSuccess(), "Запрос не был выполнен успешно");

    }

    @Step("Получение списка деталей у одной сверки")
    public static void getDetailsList(){
        System.out.println("getDetailsList");
        Response response;
        response = given().filter(sf).
                contentType(ContentType.JSON).
                with().
                queryParam("", "").
                queryParam("", "").
                queryParam("", "").
                queryParam("", "").
                get(SDC_TERMINAL_ATM_RECONCILIATION_DETAIL_LIST_JSON);
        result = response.then().
                statusCode(200).log().all()
                .extract().
                /**
                 * Это объект класса ReconciliationDetail в который собираются все атрибуты
                 * и передаются в переменную static ListResultData<ReconciliationDetail> result;
                 * для использования в других классах
                 */
                        as(new TypeRef<ListResultData<ReconciliationDetail>>());
    }
}
