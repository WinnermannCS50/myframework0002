package at.common.Config;

import com.google.common.collect.Maps;
import io.restassured.filter.Filter;
import io.restassured.specification.RequestSpecification;

import java.util.Map;
import static io.restassured.RestAssured.*;

/**
 * Класс стандартной спецификации запроса RestAssured
 */

public class CommonSpec {
    private static final Map<String, Filter> filters = Maps.newHashMap();

    private CommonSpec(){

    }
    public static Filter getFilterFor(String module){
        return filters.computeIfAbsent(module, SDCLogin::login);
    }

    public static RequestSpecification get(String module){
        baseURI = Config.getSDCHost();
        port = Config.getSDCPort();
        return expect().log().all().given().filter(getFilterFor(module)).log().all();
    }
}
